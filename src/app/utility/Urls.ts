import { environment } from 'src/environments/environment';

// All API_URLS used in the Vestige Admin Panel


export const ServiceEnum = {
  Otp: (environment.API_URLS_ENDPOINT + 'generateotp'/*:ServiceType*/),
  Verifyotp: (environment.API_URLS_ENDPOINT + 'verifyotp'/*:ServiceType*/),
  register: (environment.API_URLS_ENDPOINT + 'register'/*:ServiceType*/),
  getvendor: (environment.API_URLS_ENDPOINT + 'vendor/detail'/*:ServiceType*/),  
  getvendorcat: (environment.API_URLS_ENDPOINT + 'vendorcategories'/*:ServiceType*/),
  getamenities: (environment.API_URLS_ENDPOINT + 'amenities'/*:ServiceType*/),
  getLanguages: (environment.API_URLS_ENDPOINT + 'get-language'/*:ServiceType*/),
  getTutors: (environment.API_URLS_ENDPOINT + 'tutor_search'/*:ServiceType*/),
  gettutorinfo: (environment.API_URLS_ENDPOINT + 'tutorprofile'/*:ServiceType*/),
  follow: (environment.API_URLS_ENDPOINT + 'follow'/*:ServiceType*/),
  unfollow: (environment.API_URLS_ENDPOINT + 'unfollow'/*:ServiceType*/),
  wizpasstype: (environment.API_URLS_ENDPOINT + 'wizpasstype'),
  wizpassplan: (environment.API_URLS_ENDPOINT + 'wizpassplan'/*:ServiceType*/),
  getallcourses: (environment.API_URLS_ENDPOINT + 'getallcourses'/*:ServiceType*/),

}

