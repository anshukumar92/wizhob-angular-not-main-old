import {
    MONTH_LIST
} from 'src/app/utility/Constant';

/**
 * @function 
 * @param array => array
 * @param value =>object
 * @description remove particular object from the array
 */
export function removeObjectFromArray(array, value) {
    for (let i = 0; i < array.length; i++) {
        if (array[i].id === 0) {
            array.splice(i, 1)
        }
    }
    return array;
}

/**
 * @function formatDate format date into readable format
 * @param data isoString date
 * @description converts the ISOString date into readable format
 * @returns return date into readable format
 */
export function formatDate(date) {
    return `${MONTH_LIST[new Date(date).getMonth()]} ${new Date(date).getDate()}, ${new Date(date).getFullYear()}`;
}

/**
 * @function search the key from the array 
 * @param value { object which you want to search } array { array in the object you will search }
 * @description return object if value exists otherwise return null
*/
export function searchFromArray(value, array) {
    let results = {};
    array.map(object => {
        for (let key in object) {
            (object[key] === value) ?
                results = object : null
        }
    });
    return results;
}

