import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserDataService {
  
 httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };


  constructor(private http : HttpClient) { }

  getUserData(user:any): Observable<any>{
    return this.http.post( environment.API_URLS_ENDPOINT+ "getuserdetails" , user , this.httpOptions);
  }

  updateUserData( updatedUserData:any ):Observable<any>{
    //console.log(updatedUserData);
    return this.http.post(environment.API_URLS_ENDPOINT+"updateuser", updatedUserData);
  }

}
