import { Injectable } from '@angular/core';
import * as Urls from 'src/app/utility/Urls';
import { ApiService } from 'src/app/services/api.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class StudioService {

  constructor( private apiService: ApiService,
    private router: Router) { }

getvendor(params) { 
  return this.apiService.post(Urls.ServiceEnum.getvendor, params);
} 

getAmenities() { 
  return this.apiService.get(Urls.ServiceEnum.getamenities);
}

getVendorCat(){
  return this.apiService.get(Urls.ServiceEnum.getvendorcat);
}


}
