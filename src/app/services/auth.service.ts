import { Injectable } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Observable, BehaviorSubject } from 'rxjs';
import * as Urls from 'src/app/utility/Urls';
import { ApiService } from 'src/app/services/api.service';
import { LocalStorageService } from 'src/app/services/local-Storage';
import * as myGlobals from 'src/app/services/globals';

@Injectable({
  providedIn: 'root'
})
export class AuthServices {
  loginUrl: String = '';

  private isLogin = new BehaviorSubject<boolean>(false);

  constructor( private apiService: ApiService,
                private localStorage: LocalStorageService,
               private router: Router) { }

  getToken() {
    const token = this.localStorage.get(myGlobals.STORAGE_KEYS.TOKEN_KEY);
    return token;
  }

  setIsLogin(val: boolean){
   if(!val){
      this.localStorage.remove(myGlobals.STORAGE_KEYS.TOKEN_KEY);
      this.localStorage.remove(myGlobals.STORAGE_KEYS.USER_ID);
      this.localStorage.remove(myGlobals.STORAGE_KEYS.USER_ROLE);
      localStorage.removeItem('mobile');
    }
    this.isLogin.next(val);
  }

  getIsLogin(){
    return this.isLogin;
  }

  otpOnMobile(num: any) { 
    return this.apiService.post(Urls.ServiceEnum.Otp, num);
  }

  verifyOtp(otp: any) { 
    return this.apiService.post(Urls.ServiceEnum.Verifyotp, otp);
  }

  register(data: any) {
    return this.apiService.post(Urls.ServiceEnum.register, data);
  }

  public isAuthenticated(){
    if (this.getToken()) {
      return true;
    }
    return false;
  }

}
