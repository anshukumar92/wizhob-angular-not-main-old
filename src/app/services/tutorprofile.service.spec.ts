import { TestBed } from '@angular/core/testing';

import { TutorprofileService } from './tutorprofile.service';

describe('TutorprofileService', () => {
  let service: TutorprofileService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TutorprofileService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
