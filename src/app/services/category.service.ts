import { Injectable } from '@angular/core';
import * as Urls from 'src/app/utility/Urls';
import { ApiService } from 'src/app/services/api.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private apiService: ApiService,
    private router: Router) { }
  

getAllCategory(params) { 
    
    return this.apiService.get(Urls.ServiceEnum.getvendorcat);
  }
  getAllCources(params){
  
  	 return this.apiService.post(Urls.ServiceEnum.getallcourses, params);
  }

}
