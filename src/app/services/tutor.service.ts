import { Injectable } from '@angular/core';
import * as Urls from 'src/app/utility/Urls';
import { ApiService } from 'src/app/services/api.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class TutorService {

  constructor(private apiService: ApiService,
    private router: Router) { }

    gettutor(params) { 
      return this.apiService.post(Urls.ServiceEnum.getTutors, params);
    } 
    
    getLanguages(){
      return this.apiService.get(Urls.ServiceEnum.getLanguages);
    }
    
    getVendorCat(){
      return this.apiService.get(Urls.ServiceEnum.getvendorcat);
    }    
}
