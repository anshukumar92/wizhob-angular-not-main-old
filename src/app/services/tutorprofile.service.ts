import { Injectable } from '@angular/core';
import * as Urls from 'src/app/utility/Urls';
import { ApiService } from 'src/app/services/api.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class TutorprofileService {

  constructor(private apiService: ApiService,
    private router: Router) { }

  gettutorinfo(params) { 
    console.log('======== Tutor Service file =====> ', params);
    return this.apiService.post(Urls.ServiceEnum.gettutorinfo, params);
  }
  
  follow(params){
    return this.apiService.post(Urls.ServiceEnum.follow, params);
  }

  unFollow(params){
    return this.apiService.post(Urls.ServiceEnum.unfollow, params);
  }
}
