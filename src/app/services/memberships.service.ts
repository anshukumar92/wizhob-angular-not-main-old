import { Injectable } from '@angular/core';
import * as Urls from 'src/app/utility/Urls';
import { ApiService } from 'src/app/services/api.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class MembershipService {

  constructor(private apiService: ApiService,
    private router: Router) { }
  

  getMembership(params) { 
    
    return this.apiService.get(Urls.ServiceEnum.wizpasstype);
  }

  getPassPlan(params) { 
    
    return this.apiService.post(Urls.ServiceEnum.wizpassplan,params);
  }


}
