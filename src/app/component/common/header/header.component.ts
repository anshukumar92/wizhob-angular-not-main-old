import { Component, OnInit } from '@angular/core';
import { AuthServices } from 'src/app/services/auth.service';
import { LocalStorageService } from 'src/app/services/local-Storage';
import * as myGlobals from 'src/app/services/globals';
import { UserDataService } from '../../../services/user-data.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  isLogin:boolean = false;
  userProfileAddress:string = "https://wizhob.com/upload/users/images/default.jpeg";
  usersrtname:string = "usersnm";
  userDetail: any;
  
  constructor(
    private userDataApi : UserDataService,
    private authservice: AuthServices,
    private localStorage: LocalStorageService,
  ) { }

  ngOnInit(): void {

   this.islogin();

   this.authservice.getIsLogin().subscribe(result => {
    this.isLogin = result;
   });
  }

  logOut(){
    this.authservice.setIsLogin(false);
  }

  islogin(){
    if(this.localStorage.get(myGlobals.STORAGE_KEYS.TOKEN_KEY)){
      this.authservice.setIsLogin(true);
      this.setUserData();
    }
  }

  setUserData(){

    let val = {
      "id" :  this.localStorage.get(myGlobals.STORAGE_KEYS.USER_ID),
      "user_type" : this.localStorage.get(myGlobals.STORAGE_KEYS.USER_ROLE)
    }

    this.userDataApi.getUserData(val).subscribe(result => {
      if(result.status == 200){
        this.userDetail = result;
        this.userProfileAddress = this.userDetail.data.image?this.userDetail.data.image:"https://wizhob.com/upload/users/images/";
        this.usersrtname = this.userDetail.data.name;
        

      }
    })
  }
}
