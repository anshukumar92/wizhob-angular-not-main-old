import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthServices } from 'src/app/services/auth.service';
import { AlertService } from 'src/app/services/alert.service';
import { LocalStorageService } from 'src/app/services/local-Storage';
import * as myGlobals from 'src/app/services/globals';
import { Router } from '@angular/router';
import { AuthService, SocialUser} from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";
 

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  verifyForm: FormGroup;
  generateForm: FormGroup;
  signupForm: FormGroup;
  verifyotp: boolean;
  signup: boolean;
  sendsms: boolean;
  showLoader = false;
  error: any = null;
  genders=['male','female'];
  fileData: File = null;
  previewUrl:any = null;
  isProfileUploaded: boolean = false;
  user:any = SocialUser;
  isImageUploaded: boolean;


  constructor(private formBuilder: FormBuilder,
    private localservice: LocalStorageService,
    private authServices: AuthServices,
    private router: Router,
    private alertService: AlertService,
    private authService: AuthService ) { 

    this.buildForm();
    this.verificationForm();
    this.userSignUpForm();
  }

  ngOnInit(): void {
    this.verifyotp = false;
    this.signup    = false;
    this.sendsms   = true;
  }

  ResendOtpFunc() {
    this.onSubmit();
  }

  buildForm() {
    this.generateForm = this.formBuilder.group({
      mobile: [null, Validators.required]
    });
  }  

  verificationForm() {
    this.verifyForm = this.formBuilder.group({
      otp: [null, Validators.compose([Validators.required])]
    });
  }

  userSignUpForm() {
    this.signupForm = this.formBuilder.group({
      name: ['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.required])],
      location: ['', Validators.compose([Validators.required])],
      dateofbirth: ['', Validators.compose([Validators.required])],
      imageName: [''],
      gender: ['', Validators.compose([Validators.required])]
    });
  }

  onSubmit() {
    this.showLoader = true;
    if (!this.generateForm.valid) {
      return;
    }
    this.authServices.otpOnMobile(this.generateForm.value)
      .subscribe((data) => {
        localStorage.setItem('mobile', this.generateForm.value.mobile);
        this.verifyotp = true;
        this.sendsms   = false;
        this.signup    =  false;  
        this.showLoader = false;
      },
      error => {
        this.alertService.error('Send sms failed');
        this.showLoader = false;
      });
  }  


  verfySubmit() {
    this.showLoader = true;
    if (!this.verifyForm.valid) {
      return;
    }
    this.authServices.verifyOtp(this.verifyForm.value)
      .subscribe((res) => { 
        console.log(res, "verify response");
      if(res.body.status == 202){
        this.localservice.set(myGlobals.STORAGE_KEYS.TOKEN_KEY, res.body.data.original.access_token);
        this.localservice.set(myGlobals.STORAGE_KEYS.USER_ID, res.body.data.original.id);
        this.localservice.set(myGlobals.STORAGE_KEYS.USER_ROLE, res.body.data.original.role);
        this.authServices.setIsLogin(true);
        this.router.navigate(['/studio']);
        this.verifyotp = false;
        this.sendsms   = false; 
        this.signup    = false; 
      }else if(res.body.status == 200){
        this.verifyotp = false;
        this.sendsms   = false; 
        this.signup    = true; 
      }else{
        this.error     = res.body.message;
        this.verifyotp = true;
        this.sendsms   = false; 
        this.signup    = false; 
      }
      this.showLoader = false;
      },
      error => {
        this.alertService.error('Verification failed');
        this.showLoader = false;
      });
  }  

  register() {
    this.showLoader = true;
    if (!this.signupForm.valid) {
           this.signupForm.controls['name'].markAsTouched();
           this.signupForm.controls['dateofbirth'].markAsTouched();
           this.signupForm.controls['email'].markAsTouched();
           this.signupForm.controls['location'].markAsTouched();
           this.signupForm.controls['profileImage'].markAsTouched();
           this.signupForm.controls['gender'].markAsTouched();
      return;
    }
    this.signupForm.value['user_type'] = 3;
    this.signupForm.value['mobile'] = localStorage.getItem('mobile');
    this.signupForm.value['imageFileName'] = this.isImageUploaded?this.fileData.name:"https://wizhob.com/upload/users/images/default.jpeg";
    this.signupForm.value['image'] = this.isImageUploaded?this.fileData:"https://wizhob.com/upload/users/images/default.jpeg";

    console.log(this.signupForm.value['image'])
    this.authServices.register(this.signupForm.value)
      .subscribe((res) => {
        this.verifyotp = false;
        this.sendsms   = false;  
        if(res.body.status == 200){
          this.localservice.set(myGlobals.STORAGE_KEYS.TOKEN_KEY, res.body.data.original.access_token);
          this.localservice.set(myGlobals.STORAGE_KEYS.USER_ID, res.body.data.original.id);
          this.localservice.set(myGlobals.STORAGE_KEYS.USER_ROLE, res.body.data.original.role);
          this.authServices.setIsLogin(true);
          this.router.navigate(['/studio']);
          this.signup    = false; 
        }else{
          this.error     = res.body.message;
          this.signup    = true; 
        }
      },
      error => {
        this.showLoader = false;
        this.alertService.error('Verification failed');
      });
  }  

  onFileChange(fileInput: any){
    if(!fileInput.target.files[0])
        return
    
    this.fileData = <File>fileInput.target.files[0];
    this.preview();
  }

  preview() {
      // Show preview 
      var mimeType = this.fileData.type;
      if (mimeType.match(/image\/*/) == null) {
        return;
      }
   
      var reader = new FileReader();      
      reader.readAsDataURL(this.fileData); 
      this.isProfileUploaded = true;
      reader.onload = (_event) => { 
        this.previewUrl = reader.result; 
      }
      this.isImageUploaded = true;
  }


  // social login
  signInWithSocialAcc(Provider: string): void {

    let socialProvider: string;
    
    if(Provider == "facebook"){
      socialProvider = FacebookLoginProvider.PROVIDER_ID;
    }else if(Provider == "google"){
      socialProvider = GoogleLoginProvider.PROVIDER_ID;
    }

    this.authService.signIn(socialProvider).then(socialUser => {
      this.user = socialUser;
      console.log(socialUser);
    });

  }
 
  signOut(): void {
    this.authService.signOut();
  }
}
