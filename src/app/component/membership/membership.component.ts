import { Component, OnInit } from '@angular/core';
import { MembershipService } from '../../services/memberships.service';
import { LocalStorageService } from 'src/app/services/local-Storage';
import * as myGlobals from 'src/app/services/globals';
import {MatSnackBar} from '@angular/material/snack-bar';
import { Router } from '@angular/router';


@Component({
  selector: 'app-membership',
  templateUrl: './membership.component.html',
  styleUrls: ['./membership.component.css','./membershipdesktop.min.css','./style.min.css']
})
export class MembershipComponent implements OnInit {
  

  public membershipArr = [];
  public planArr = [];
  public params = {};
  public currplan='';
  public selectedItem='';
  public selectedPlan='';

  public sigbleOrderPrice='';
  public sigbleOrderDuration='';
  public sigbleOrderPlan='';
  


  constructor(
    private router: Router,
    private packageDataApi : MembershipService,
    
    private localservice: LocalStorageService,
    private _snackBar: MatSnackBar) { }
    
  ngOnInit(): void {

      let dataid = {"id":this.localservice.get(myGlobals.STORAGE_KEYS.USER_ID)};

    this.packageDataApi.getMembership(dataid).subscribe(result => {
      if(result.status == 200){
        this.membershipArr = result.body.data;
        var f_id = result.body.data[0].pass_id ;
        var f_name =result.body.data[0].pass_type;
        this.firstrun(f_id,f_name);
      }
      
    });
        
    }


  firstrun(f_id,f_name){
    
    this.params['pass_type_id'] = f_id;
       this.packageDataApi.getPassPlan(this.params).subscribe(result => {
        
        if(result.status == 200){

        this.selectedItem =f_name;
        this.currplan= f_name;
        
        this.planArr = result.body.data;
        
        }
      
      });

  }  

   memberSelect(event,newValue) {

       this.selectedItem = newValue;
       
       this.params['pass_type_id'] = event;
       this.packageDataApi.getPassPlan(this.params).subscribe(result => {
        
        if(result.status == 200){
        this.currplan=newValue;
        
        this.planArr = result.body.data;
        
        }
      
      });


  }

    getPlanbal(allBal,selectBal){

        this.params['pass_type_id'] = allBal;
       this.packageDataApi.getPassPlan(this.params).subscribe(result => {
        
          if(result.status == 200){
      let num=result.body.data;      
        
        num.forEach(function (value) {
          
          if(value.id==selectBal){
          
          this.sigbleOrderPrice=value.pass_price;
          this.sigbleOrderDuration=value.pass_duration;
          this.sigbleOrderPlan=this.selectedItem;
          
          
          }
        }.bind(this)); 

        
        
        }
      
      });

    }

 planSelect(eventd) {

       this.selectedPlan = eventd;
    }    



}
