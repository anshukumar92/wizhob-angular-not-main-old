import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FitcoachComponent } from './fitcoach.component';

describe('FitcoachComponent', () => {
  let component: FitcoachComponent;
  let fixture: ComponentFixture<FitcoachComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FitcoachComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FitcoachComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
