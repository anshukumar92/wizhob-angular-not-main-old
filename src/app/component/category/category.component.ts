import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../../services/category.service';
import { LocalStorageService } from 'src/app/services/local-Storage';
import * as myGlobals from 'src/app/services/globals';
import {MatSnackBar} from '@angular/material/snack-bar';
import { Router } from '@angular/router';


@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css','./site.min.css']
})
export class CategoryComponent implements OnInit {
  

public cateArr = [];
public courceArr = [];
public params = {};  
  constructor(
    private router: Router,
    private packageDataApi : CategoryService,
    
    private localservice: LocalStorageService,
    private _snackBar: MatSnackBar) { }
    
  ngOnInit(): void {

      let dataid = {"id":this.localservice.get(myGlobals.STORAGE_KEYS.USER_ID)}; 

     this.packageDataApi.getAllCategory(dataid).subscribe(result => {
      if(result.status == 200){
        this.cateArr = result.body.categories;
        
        this.asyncPromise().then(); //Print Hello after 2s            
      }
      
    });       
      

      this.params['sortby'] = 0;
      this.params['start'] = 0;
      this.params['limit'] = 30;
    this.packageDataApi.getAllCources(this.params).subscribe(result => {
      if(result.status == 200){
        this.courceArr = result.body.data;
        
        
      }
      
    });


    }
    functioncallback(){
    ma5menu({
                menu: '.site-menu',
                activeClass: 'active',
                footer: '#ma5menu-tools',
                position: 'left',
                closeOnBodyClick: true
            });
            }


asyncPromise() {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve("Hello");
        this.functioncallback();
      }, 2000)
    })
}

  
    catSelect(allBal){
    alert(allBal);
      this.params['category'] = allBal;
      this.params['sortby'] = 0;
      this.params['start'] = 0;
      this.params['limit'] = 30;
    
      this.packageDataApi.getAllCources(this.params).subscribe(result => {
      if(result.status == 200){
        this.courceArr = result.body.data;
        }

       }); 

    }
}
