import { Component, ViewChild, EventEmitter, NgZone, OnInit } from '@angular/core';
import { StudioService } from 'src/app/services/studio.service';

@Component({
  selector: 'app-studio',
  templateUrl: './studio.component.html',
  styleUrls: ['./studio.component.css']
})
export class StudioComponent implements OnInit {
  address: Object;
  phone: string;
  formattedAddress: string;
  public params = {};
  public vendordetails = [];
  public amenities = [];
  public vendorcat = [];
  public amenitiesOptions = [];
  public catogoryOptions = [];
  public sortby = 0;
  public lat = 28.491700;
  public lng = 77.094;
  constructor(
    public zone: NgZone,
    private studioservice: StudioService
  ) { }

  ngOnInit(): void {
    this.getVendorData();
    this.getVendorCat();
    this.getAmenities();
  }

  sortbyChange(event){
    this.sortby = event.target.value;
    this.getVendorData();
  }

  categoryChange(event) {
    if(event.target.checked){
      this.catogoryOptions.push(event.target.value);
    }else{
      const index: number = this.catogoryOptions.indexOf(event.target.value);
      if (index !== -1) {
          this.catogoryOptions.splice(index, 1);
      } 
    }
    console.log('Categories selected => ', this.catogoryOptions, ' <= Categories selected');
  }

  aminitiesChange(event) {
    if(event.target.checked){
      this.amenitiesOptions.push(event.target.value);
    }else{
      const index: number = this.amenitiesOptions.indexOf(event.target.value);
      if (index !== -1) {
          this.amenitiesOptions.splice(index, 1);
      } 
    }
    console.log('Aminitiese selected => ', this.amenitiesOptions, ' <= Aminitiese selected');
  }

  dateChange(event) {
    this.getVendorData();
  }

  filtervendor() {
    this.getVendorData();
  }

  getAddress(place: object) {
    this.lat = place['geometry']['location'].lat();
    this.lng = place['geometry']['location'].lng();
    this.address = place['formatted_address'];
    this.phone = this.getPhone(place);
    this.formattedAddress = place['formatted_address'];
    this.zone.run(() => this.formattedAddress = place['formatted_address']);
    this.getVendorData();
  }

  getAddrComponent(place, componentTemplate) {
    let result;
    for (let i = 0; i < place.address_components.length; i++) {
      const addressType = place.address_components[i].types[0];
      if (componentTemplate[addressType]) {
        result = place.address_components[i][componentTemplate[addressType]];
        return result;
      }
    }
    return;
  }

  getPhone(place) {
    const COMPONENT_TEMPLATE = { formatted_phone_number: 'formatted_phone_number' },
      phone = this.getAddrComponent(place, COMPONENT_TEMPLATE);
    return phone;
  }

  getVendorData() {
    this.params['latitude'] = this.lat;
    this.params['longitude'] = this.lng;
    this.params['amenities'] = this.amenitiesOptions.join(',');
    this.params['category'] = this.catogoryOptions.join(',');
    this.params['sortby'] = this.sortby;
    this.params['customer_id'] = 54;
    this.params['limit'] = 10;
    this.studioservice.getvendor(this.params).subscribe(res => {
      console.log(res);
      this.vendordetails = res.body.data;
      console.log(this.vendordetails);
    });
  }

  getAmenities() {
    this.studioservice.getAmenities().subscribe(res => {
      this.amenities = res.body.data;
      console.log(this.amenities);
    });
  }

  getVendorCat() {
    this.studioservice.getVendorCat().subscribe(res => {
      this.vendorcat = res.body.categories;
      console.log(this.vendorcat);
    });
  }

}
