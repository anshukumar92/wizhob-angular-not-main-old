import { Component, ViewChild, EventEmitter, NgZone, OnInit } from '@angular/core';
import { TutorService } from 'src/app/services/tutor.service';

@Component({
  selector: 'app-tutor',
  templateUrl: './tutor.component.html',
  styleUrls: ['./tutor.component.css']
})
export class TutorComponent implements OnInit {
  address: Object;
  phone: string;
  formattedAddress: string;
  public params = {};
  public tutordetails = [];
  public languages = [];
  public vendorcat = [];
  public languagesOptions = [];
  public catogoryOptions = [];
  public sortby = 0;
  public lat = 28.491700;
  public lng = 77.094;
  constructor(
    public zone: NgZone,
    private tutorsservice: TutorService
  ) { }

  ngOnInit(): void {
    this.getTutorData();
    this.getVendorCat();
    this.getLanguages();
  }

  sortbyChange(event){
    this.sortby = event.target.value;
    this.getTutorData();
  }

  categoryChange(event) {
    if(event.target.checked){
      this.catogoryOptions.push(event.target.value);
    }else{
      const index: number = this.catogoryOptions.indexOf(event.target.value);
      if (index !== -1) {
          this.catogoryOptions.splice(index, 1);
      } 
    }
    console.log('Categories selected => ', this.catogoryOptions, ' <= Categories selected');
  }

  languageChange(event) {
    if(event.target.checked){
      this.languagesOptions.push(event.target.value);
    }else{
      const index: number = this.languagesOptions.indexOf(event.target.value);
      if (index !== -1) {
          this.languagesOptions.splice(index, 1);
      } 
    }
    console.log('languages selected => ', this.languagesOptions, ' <= languages selected');
  }

  dateChange(event) {
    this.getTutorData();
  }

  filtervendor() {
    this.getTutorData();
  }

  getAddress(place: object) {
    this.lat = place['geometry']['location'].lat();
    this.lng = place['geometry']['location'].lng();
    this.address = place['formatted_address'];
    this.phone = this.getPhone(place);
    this.formattedAddress = place['formatted_address'];
    this.zone.run(() => this.formattedAddress = place['formatted_address']);
    this.getTutorData();
  }

  getAddrComponent(place, componentTemplate) {
    let result;
    for (let i = 0; i < place.address_components.length; i++) {
      const addressType = place.address_components[i].types[0];
      if (componentTemplate[addressType]) {
        result = place.address_components[i][componentTemplate[addressType]];
        return result;
      }
    }
    return;
  }
  abc(abc){
    return abc.replace('+', ' ');
  }
  getPhone(place) {
    const COMPONENT_TEMPLATE = { formatted_phone_number: 'formatted_phone_number' },
      phone = this.getAddrComponent(place, COMPONENT_TEMPLATE);
    return phone;
  }

  getTutorData() {
    this.params['latitude'] = this.lat;
    this.params['longitude'] = this.lng;
    this.params['start'] = 0;
    this.params['language'] = this.languagesOptions.join(',');
    this.params['category'] = this.catogoryOptions.join(',');
    this.params['sortby'] = this.sortby;
    this.params['customer_id'] = 1;
    this.params['limit'] = 10;
    this.tutorsservice.gettutor(this.params).subscribe(res => {
      this.tutordetails = res.body.data;
      console.log(this.tutordetails);
    });
  }

  getLanguages() {
    this.tutorsservice.getLanguages().subscribe(res => {
      this.languages = res.body.data;
      console.log(this.languages);
    });
  }

  getVendorCat() {
    this.tutorsservice.getVendorCat().subscribe(res => {
      this.vendorcat = res.body.categories;
      console.log(this.vendorcat);
    });
  }

}
