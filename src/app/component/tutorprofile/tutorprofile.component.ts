import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TutorprofileService } from 'src/app/services/tutorprofile.service';

@Component({
  selector: 'app-tutorprofile',
  templateUrl: './tutorprofile.component.html',
  styleUrls: ['./tutorprofile.component.css']
})
export class TutorprofileComponent implements OnInit {
  id: Number;
  name: String;
  profession: String;
  isFollow : boolean;
  followCount: number;
  public tutordata = [];
  public tutorinfo = [];

  constructor(
    private route: ActivatedRoute, 
    private tutorprofilesservice: TutorprofileService
    ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.id = this.route.snapshot.params.id;
      this.name = this.route.snapshot.params.name;
      this.profession = this.route.snapshot.params.profession;
    });

    this.gettutorData();
  }

  followToTutor(){
    let tutorpro = {
      "followed_to_id": this.id,
      "followed_by_id": 55
    };

    this.tutorprofilesservice.follow(tutorpro).subscribe(res => {
      this.isFollow = true;
      var count = this.followCount;
      this.followCount = count + 1;
      console.log(this.isFollow, '======Follow APIs Response =======> ', res.body.data);
    });
  }

  unFollowToTutor(){
    let tutorpro = {
      "followed_to_id": this.id,
      "followed_by_id": 55
    };
    
    this.tutorprofilesservice.unFollow(tutorpro).subscribe(res => {
      this.isFollow = false;
      var count = this.followCount;
      this.followCount = count - 1;
      console.log(this.isFollow, '======UnFollow APIs Response =======> ', res.body.data);
    });
  }

  gettutorData(){
    let tutorpro = {
                "id": this.id, 
                "customer_id":55,
                "name" : this.name,
                "profession" : this.profession
              };
    this.tutordata.push(tutorpro);
    this.tutorprofilesservice.gettutorinfo(this.tutordata[0]).subscribe(res => {
        this.tutorinfo = res.body.data;
        this.isFollow = res.body.data.isFollow;
        this.followCount = res.body.data.followers;
        console.log('======APIs Response =======> ', this.tutorinfo);
    });
    
  }

}
