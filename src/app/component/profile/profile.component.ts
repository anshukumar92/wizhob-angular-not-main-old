import { Component, OnInit } from '@angular/core';
import { UserDataService } from '../../services/user-data.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LocalStorageService } from 'src/app/services/local-Storage';
import * as myGlobals from 'src/app/services/globals';
import {MatSnackBar} from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  userDetail: any;
  val: any;
  editDetailForm: FormGroup;
 // previewUrl:string= "../../../../assets/img/signupuser.jpg";
  isProfileUploaded:boolean =false;
  fileData: any;
  previewUrl:any = null;
  isImageUploaded: boolean;

  constructor(
    private router: Router,
    private userDataApi : UserDataService,
    private formBuilder: FormBuilder,
    private localservice: LocalStorageService,
    private _snackBar: MatSnackBar) { }

  userSignUpForm() {
    this.editDetailForm = this.formBuilder.group({
      name: ['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.required])],
      location: ['', Validators.compose([Validators.required])],
      dateofbirth: ['', Validators.compose([Validators.required])],
      image: [''],
      gender: ['', Validators.compose([Validators.required])]
    });
  }

  ngOnInit(): void {
    
    this.  userSignUpForm();

    this.val = {
      "id" :  this.localservice.get(myGlobals.STORAGE_KEYS.USER_ID),
      "user_type" : this.localservice.get(myGlobals.STORAGE_KEYS.USER_ROLE)
    }

    this.userDataApi.getUserData(this.val).subscribe(result => {
      if(result.status == 200){
        this.userDetail = result;

        this.isProfileUploaded = true;
        this.previewUrl = this.userDetail.data.image;

        this.editDetailForm.patchValue({
          name: this.userDetail.data.name,
          email: this.userDetail.data.email,
          location: this.userDetail.data.location,
          dateofbirth: this.userDetail.data.dob,
          gender: this.userDetail.data.gender
        });
      }
    })
  }

  editProfile(){ 


    var formData = new FormData();
    

      console.log(this.editDetailForm.value);

     this.editDetailForm.value.user_id = this.localservice.get(myGlobals.STORAGE_KEYS.USER_ID);
     this.editDetailForm.value.user_type = this.localservice.get(myGlobals.STORAGE_KEYS.USER_ROLE);
     this.editDetailForm.value.mobile = localStorage.getItem('mobile');
     this.isProfileUploaded?this.editDetailForm.value.image = this.fileData:this.editDetailForm.value.image = this.previewUrl;
    
     formData.append('user_id',this.editDetailForm.value.user_id);
     formData.append('user_type',this.editDetailForm.value.user_type);
     formData.append('mobile',this.editDetailForm.value.mobile);
     formData.append('email',this.editDetailForm.value.email);
     formData.append('image',this.editDetailForm.value.image);
     formData.append('name', this.editDetailForm.value.name);
     formData.append("gender",this.editDetailForm.value.gender);
     formData.append("location",this.editDetailForm.value.location);
     formData.append("dateofbirth",this.editDetailForm.value.dateofbirth);
      
    this.userDataApi.updateUserData(formData).subscribe(result => {
      //console.log(result);
      this.openSnackBar(result.message, "undo");
      this.router.navigate(['/profile']);
    });
  }


  onFileChange(fileInput: any){
    if(!fileInput.target.files[0])
        return;
    
    this.fileData = fileInput.target.files[0];
    this.preview();
  }

  preview() {
      // Show preview 
      var mimeType = this.fileData.type;
      if (mimeType.match(/image\/*/) == null) {
        return;
      }
   
      var reader = new FileReader();      
      reader.readAsDataURL(this.fileData); 
      this.isProfileUploaded = true;
      reader.onload = (_event) => { 
        this.previewUrl = reader.result; 
      }
  }

  openSnackBar(message: string , action : string) {
    this._snackBar.open(message, action, {
      duration: 2000,
   });
  }

}