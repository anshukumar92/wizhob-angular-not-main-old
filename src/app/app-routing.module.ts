import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MainComponent } from './component/main/main.component';
import { LoginComponent } from './component/auth/login/login.component';
import { StudioComponent } from './component/studio/studio.component';
import { TutorComponent } from './component/tutor/tutor.component';
import { TutorprofileComponent } from './component/tutorprofile/tutorprofile.component';
import { ChatsComponent } from './component/chats/chats.component';
import { MembershipComponent } from './component/membership/membership.component';
import { CategoryComponent } from './component/category/category.component';
import { AboutComponent } from './component/about/about.component';
import { CareersComponent } from './component/careers/careers.component';
import { TeamComponent } from './component/team/team.component'; 
import { ContactComponent } from './component/contact/contact.component'; 
import { CorporatesComponent } from './component/corporates/corporates.component';
import { FitcoachComponent } from './component/fitcoach/fitcoach.component';
import { AuthGuardService as AuthGuard, LoginFlowGuard, DashboardFlowGuard } from 'src/app/guards/auth-guard.service';
import { ProfileComponent } from './component/profile/profile.component';


export const routes: Routes = [
  {
    path: '', component: MainComponent,  data: { title: 'Wizhob' },
    children: [
      // use when login authguard implement
      
    ]
  },
  {
    path: 'login',
    component: LoginComponent,
   // canActivate: [DashboardFlowGuard],
    data: { title: 'Login' }
  },
  {
    path: 'studio',
    component: StudioComponent,
    canActivate: [DashboardFlowGuard],
    data: { title: 'Studio' }
  },
  {
    path: 'experts',
    component: TutorComponent,
    canActivate: [DashboardFlowGuard],
    data: { title: 'Tutor' }
  },
  {
    path: 'expert/:profession/:id/:name',
    component: TutorprofileComponent,
    canActivate: [DashboardFlowGuard],
    data: { title: 'Tutor Profile' }
  },
  {
    path: 'messages',
    component: ChatsComponent,
    canActivate: [DashboardFlowGuard],
    data: { title: 'Messages' }
  },
  {
    path: 'membership',
    component: MembershipComponent,
    canActivate: [DashboardFlowGuard],
    data: { title: 'Membership' }
  },
  {
    path: 'category',
    component: CategoryComponent,
    canActivate: [DashboardFlowGuard],
    data: { title: 'Select Category' }
  },

  {
    path: 'about',
    component: AboutComponent,
    data: { title: 'About' }
  },
  {
    path: 'team',
    component: TeamComponent,
    canActivate: [DashboardFlowGuard],
    data: { title: 'Team' }
  },
  {
    path: 'careers',
    component: CareersComponent,
    canActivate: [DashboardFlowGuard],
    data: { title: 'Careers' }
  },
  {
    path: 'corporate',
    component: CorporatesComponent,
    canActivate: [DashboardFlowGuard],
    data: { title: 'Corporate' }
  },
  {
    path: 'contact',
    component: ContactComponent,
    data: { title: 'Contact' }
  },
  {
    path: 'fitcoach',
    component: FitcoachComponent,
    canActivate: [DashboardFlowGuard],
    data: { title: 'Fitcoach' }
  },
  {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [DashboardFlowGuard],
    data: { title : 'profile'}
  }   

];

@NgModule({
  imports: [
    RouterModule.forRoot(
      routes,
      // { enableTracing: true }
    )
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
