import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AutocompleteComponent } from './google-places.component';
import { AgmCoreModule } from '@agm/core';
import { MatGoogleMapsAutocompleteModule } from '@angular-material-extensions/google-maps-autocomplete';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatRadioModule } from '@angular/material/radio';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HeaderComponent } from './component/common/header/header.component';
import { FooterComponent } from './component/common/footer/footer.component';
import { MainComponent } from './component/main/main.component';
import { AppRoutingModule } from './app-routing.module';
import { AuthComponent } from './component/auth/auth.component';
import { AuthheaderComponent } from './component/auth/common/authheader/authheader.component';
import { AuthfooterComponent } from './component/auth/common/authfooter/authfooter.component';
import { StudioComponent } from './component/studio/studio.component';
import { MembershipComponent } from './component/membership/membership.component';
import { CategoryComponent } from './component/category/category.component';
import { AboutComponent } from './component/about/about.component';
import { TeamComponent } from './component/team/team.component';
import { ContactComponent } from './component/contact/contact.component';
import { CareersComponent } from './component/careers/careers.component';
import { CorporatesComponent } from './component/corporates/corporates.component';
import { FitcoachComponent } from './component/fitcoach/fitcoach.component';
import { LoginComponent } from './component/auth/login/login.component';
import { SocialLoginModule, AuthServiceConfig, LoginOpt , AuthService} from "angularx-social-login";
import { GoogleLoginProvider, FacebookLoginProvider } from "angularx-social-login";
 


//services
import { StudioService } from 'src/app/services/studio.service';
import { TutorService } from 'src/app/services/tutor.service';
import { AuthServices } from 'src/app/services/auth.service';
import { ApiService } from 'src/app/services/api.service';
import { AlertService } from './services/alert.service';
import { LocalStorageService } from 'src/app/services/local-Storage';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthGuardService as AuthGuard, LoginFlowGuard, DashboardFlowGuard } from 'src/app/guards/auth-guard.service';
import { MatNativeDateModule } from '@angular/material/core';
import { InterceptorServiceService } from 'src/app/services/interceptor-service.service';
import { TutorComponent } from './component/tutor/tutor.component';
import { ProfileComponent } from './component/profile/profile.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { TutorprofileService } from 'src/app/services/tutorprofile.service';
import { TutorprofileComponent } from './component/tutorprofile/tutorprofile.component';
import { ChatsComponent } from './component/chats/chats.component';
import { MembershipService } from 'src/app/services/memberships.service';
import { CategoryService } from 'src/app/services/category.service';
// const googleLoginOptions: LoginOpt = {
//   scope: 'profile email'
// };

export function provideConfig() {  
  const config = new AuthServiceConfig(  
    [  
      {  
        id: FacebookLoginProvider.PROVIDER_ID,  
        provider: new FacebookLoginProvider('app -id')  
      },  
      {  
        id: GoogleLoginProvider.PROVIDER_ID,  
        provider: new GoogleLoginProvider('896251621676-e6ljq9gl7fghdqbb3cd5c3b2e888imcn.apps.googleusercontent.com')  
      }  
    ]  
  );  
  return config;  
} 

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    MainComponent,
    AuthComponent,
    LoginComponent,
    AuthheaderComponent,
    AuthfooterComponent,
    StudioComponent,
    MembershipComponent,
    CategoryComponent,
    AboutComponent,
    TeamComponent,
    ContactComponent,
    CareersComponent,
    CorporatesComponent,
    FitcoachComponent,
    AutocompleteComponent,
    TutorComponent,
    TutorprofileComponent,
    ProfileComponent,
    ChatsComponent
  ],
  imports: [
    BrowserModule,
    // AgmCoreModule.forRoot({
    //   apiKey: 'AIzaSyDS4GyHhrsDHkoL4FIrIfOZCiqrbbXv4uI',
    //   libraries: ['places']
    // }),
    MatGoogleMapsAutocompleteModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatRadioModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,  
    BrowserAnimationsModule,
    MatGoogleMapsAutocompleteModule,
    SocialLoginModule,
    MatSnackBarModule
  ],
  providers: [
    AuthServices,
    TutorService,
    TutorprofileService,
    MembershipService,
    CategoryService,
    AuthGuard,
    LoginFlowGuard, 
    DashboardFlowGuard,
    ApiService,
    AlertService,
    LocalStorageService,
    AuthService,
    {
      provide: AuthServiceConfig,
      useFactory: provideConfig
    },
    StudioService,
    {
      provide : HTTP_INTERCEPTORS,
      useClass: InterceptorServiceService,
      multi : true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
